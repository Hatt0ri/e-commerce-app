import * as React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

interface IMainTooltipProps {
    body: React.ReactNode;
}
export const MainTooltip: React.FunctionComponent<IMainTooltipProps> = (props) => {
    const placement = 'top';
    const { body, children } = props;
    return (
        <OverlayTrigger
            placement={placement}
            overlay={
                <Tooltip id={`tooltip-${placement}`}>
                    {body}
                </Tooltip>
            }
        >
            {children}
        </OverlayTrigger>
    );
};
