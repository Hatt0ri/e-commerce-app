import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';

export const NextArrow: React.FunctionComponent<any> = (props) => {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style }}
            onClick={onClick}
        >
            <FontAwesomeIcon icon={faChevronRight} size={'1x'} />
        </div>
    );
};

export const PreviousArrow: React.FunctionComponent<any> = (props) => {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style }}
            onClick={onClick}
        >
            <FontAwesomeIcon icon={faChevronLeft} size={'1x'} />
        </div>
    );
};
