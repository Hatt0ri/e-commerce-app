import * as React from 'react';
import Alert, { AlertProps } from 'react-bootstrap/Alert';

interface IPageAlertProps extends AlertProps { }

const PageAlert: React.FunctionComponent<IPageAlertProps> = (props: IPageAlertProps) => {
    return (
        <Alert {...props} style={{ textAlign: 'center' }} className='py-5'>
            {props.children}
        </Alert>
    );
};

export default PageAlert;
