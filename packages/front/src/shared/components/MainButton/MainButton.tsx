import * as React from 'react';
import * as styles from './button.module.scss';
import { Link } from 'react-router-dom';
interface IMainButtonProps {
    href?: string;
    className?: string;
    to: string;
}

interface IMainButtonState { }

export default class MainButton extends React.Component<IMainButtonProps, IMainButtonState> {
    constructor(props: IMainButtonProps) {
        super(props);
    }
    public render() {
        const { children, className, ...rest } = this.props;
        return (
            <Link {...rest} className={[styles.btn, className].join(' ')}>
                {children}
            </Link>
        );
    }
}
