const price = (pricing: number) => {
    return pricing.toFixed(2).replace('.', ',');
};
export default price;
