export interface IGetAllProductsResponse {
    getAllProducts: Product[];
}
export interface IGetProductByIdResponse {
    getProductById: Product;
}

export interface Product {
    id: string;
    sku: string;
    type: string;
    title: string;
    description?: string;
    quantity: number;
    pricing: number;
    shipping?: Shipping;
    details: ProductDetails;
}

export interface ProductDetails {
    keywords: string[];
    listImage: string;
    gallery: string[];
}

export interface Dimensions {
    width: number;
    height: number;
    depth: number;
}

export interface Shipping {
    weight: number;
    dimensions: Dimensions;
}
