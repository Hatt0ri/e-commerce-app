import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './shared/scss/_sharedStyles.scss';
import 'bootstrap/scss/bootstrap.scss';
import App from './components/App/App';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { cartReducer } from './CartStore/reducers';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { ApolloProvider } from '@apollo/react-hooks';

const graphqlUri = 'http://' + process.env.SERVER_ENDPOINT + process.env.SERVER_PORT + '/graphql';

const client = new ApolloClient({
    link: ApolloLink.from([
        onError(({ graphQLErrors, networkError }) => {
            if (graphQLErrors) {
                graphQLErrors.forEach(({ message, locations, path }) =>
                    // tslint:disable-next-line:no-console
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
                    ),
                );
            }
            // tslint:disable-next-line:no-console
            if (networkError) { console.log(`[Network error]: ${networkError}`); }
        }),
        new HttpLink({
            uri: graphqlUri,
            credentials: 'same-origin',
        }),
    ]),
    cache: new InMemoryCache(),
});

const composeEnhancers = composeWithDevTools({
    latency: 0,
});

const rootReducer = combineReducers({
    cartReducer,
});
export type IGlobalState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer, composeEnhancers());

ReactDOM.render(
    <ApolloProvider client={client}>
        <Provider store={store}>
            <App />
        </Provider>
    </ApolloProvider>,
    document.getElementById('root'));
