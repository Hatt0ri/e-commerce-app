import * as React from 'react';
import { Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import * as styles from './footer.module.scss';

interface LinkType {
    to: string;
    text: string;
    disabled?: boolean;
}
export default class HeaderBottom extends React.PureComponent {

    generateButton({ to, text, disabled = false, index }: { to: string; text: string; disabled?: boolean; index: number; }) {
        return (
            <Nav.Item as={'li'}
                key={index}
            >
                <Nav.Link
                    disabled={disabled}
                    as={Link} to={to}
                >
                    {text}
                </Nav.Link>
            </Nav.Item>
        );
    }

    render() {
        const navItems: LinkType[] = [
            {
                to: '/',
                text: 'Account',
                disabled: true,
            },
            {
                to: '/products',
                text: 'Products',
                disabled: true,

            },
            {
                to: '/blog',
                text: 'Blog',
                disabled: true,

            },
            {
                to: '/about',
                text: 'About',
                disabled: true,

            },
            {
                to: '/contact',
                text: 'Contact',
                disabled: true,

            },
        ];
        return (
            <footer>
                <Nav
                    className={styles.nav}
                    // onSelect={selectedKey => alert(`selected ${selectedKey}`)}
                    as={'ul'}
                >
                    {navItems.map((item, index) => {
                        return this.generateButton({ ...item, index });
                    })}
                </Nav>
                <div className={styles.veryBottomFooter}>
                    <span>
                        2019 |  Developed by <a href='https://gitlab.com/Hatt0ri'>Hattori</a>
                    </span>
                </div>
            </footer>
        );
    }
}
