import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IGlobalState } from '../..';
import { ICart } from '../../CartStore/reducers';
import CartPage from './CartPage';
import {
  decrementProductNumberInCart,
  incrementProductNumberInCart,
  removeFromCart,
  CartActionTypes,
} from '../../CartStore/actions';

// Component

export interface ICartPageStateProps extends ICart { }

export interface ICartPageDispatchProps {
  actions: {
    onRemoveItemClick: (id: string) => void;
    onIncrementBtnClick: (id: string) => void;
    onDecrementBtnClick: (id: string) => void;
  };
}

// Container

interface ICartPageOwnProps { }

const mapStateToProps = (state: IGlobalState, ownProps: ICartPageOwnProps): ICartPageStateProps => {
  return {
    products: state.cartReducer.products,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<CartActionTypes>, ownProps: ICartPageOwnProps): ICartPageDispatchProps => {
  return {
    actions: {
      onRemoveItemClick: (id) => {
        dispatch(removeFromCart(id));
      },
      onIncrementBtnClick: (id) => {
        dispatch(incrementProductNumberInCart(id));
      },
      onDecrementBtnClick: (id) => {
        dispatch(decrementProductNumberInCart(id));
      },
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CartPage);
