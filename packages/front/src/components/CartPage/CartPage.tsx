import * as React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import * as styles from './cartPage.module.scss';
import MainButton from '../../shared/components/MainButton/MainButton';
import { ICartPageStateProps, ICartPageDispatchProps } from './CartReduxContainer';
import CartContent from './CartContent';
import price from '../../shared/price';
import PageAlert from '../../shared/components/PageAlert';

export interface ICartPageProps extends ICartPageStateProps, ICartPageDispatchProps { }

interface ICartPageState { }

export default class CartPage extends React.Component<ICartPageProps, ICartPageState> {
    rerenderContent = () => {
        this.forceUpdate();
    }

    public render() {
        const productKeys = Object.keys(this.props.products).filter((key) => {
            return this.props.products[key] !== undefined;
        });
        let subTotal = 0;
        productKeys.forEach( (key) => {
            const item = this.props.products[key];
            subTotal += item.cartQuantity * item.pricing;
        });
        return (
            <Container className={styles.containerWrapper}>
                <Row>
                    <Col sm={12}>
                        {productKeys.length > 0 ?
                            <>
                                <CartContent {...this.props} rerenderContent={this.rerenderContent} />
                                <ul className={styles.cartTotals}>
                                    <li className={styles.cartTotal}>
                                        <strong>
                                            Subtotal:
                                </strong>
                                        <div className={styles.expander} />
                                        <span>
                                            €{price(subTotal)}
                                        </span>
                                    </li>
                                    <li className={styles.cartTotal}>
                                        <strong>
                                            Shipping:
                                </strong>
                                        <div className={styles.expander} />
                                        <span>
                                            <a onClick={() => Function()}>Add Info</a>
                                        </span>
                                    </li>
                                    <li className={styles.cartTotal}>
                                        <strong>
                                            Coupon Code:
                                </strong>
                                        <div className={styles.expander} />
                                        <span>
                                            <a onClick={() => Function()}>Add Coupon</a>
                                        </span>
                                    </li>
                                    <li className={styles.cartTotal}>
                                        <strong>
                                            Grand Total:
                                </strong>
                                        <div className={styles.expander} />
                                        <span>
                                            €{price(subTotal)}
                                        </span>
                                    </li>
                                </ul>
                                <div className={styles.actionBtn}>
                                    <MainButton to='#'>Check Out</MainButton>
                                </div>
                            </>
                            :
                            <PageAlert variant={'dark'}>
                                The cart is empty.
                            </PageAlert>
                        }
                    </Col>
                </Row>
            </Container>
        );
    }
}
