import * as React from 'react';
import * as styles from './cartPage.module.scss';
import { ICartPageProps } from './CartPage';
import CartProduct from './CartProduct';

export interface ICartContentProps extends ICartPageProps {
    rerenderContent: () => void;
}

export interface ICartContentState { }

class CartContent extends React.Component<ICartContentProps, ICartContentState> {
    render() {
        const { products, actions } = this.props;
        const productKeys = Object.keys(products).filter((key) => {
            return products[key] !== undefined;
        });
        return (
            <div className={styles.cartContent}>
                <div className={styles.cart}>
                    <div className={styles.header}>
                        <div className={[styles.item, styles.firstHeaderItem].join(' ')}>Item</div>
                        <div className={styles.item}>Price</div>
                        <div className={styles.item}>Number</div>
                        <div className={styles.item}>Total</div>
                    </div>
                    {productKeys.length > 0 ?
                        productKeys.map((productId) => {
                            return (
                                <CartProduct
                                    key={productId}
                                    actions={actions}
                                    product={this.props.products[productId]}
                                    rerenderContent={this.props.rerenderContent}
                                />
                            );
                        })
                        :
                        <>The cart is empty.</>
                    }
                </div>
            </div>
        );
    }
}

export default CartContent;
