import * as React from 'react';
import * as styles from './cartPage.module.scss';
import { Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { ICartContentProps } from './CartContent';
import { ICartProduct } from '../../CartStore/reducers';

interface ICartProductProps {
    actions: ICartContentProps['actions'];
    product: ICartProduct;
    rerenderContent: () => void;
}

export default class CartProduct extends React.PureComponent<ICartProductProps> {

    OnIncrementBtnClickHandler = () => {
        this.props.actions.onIncrementBtnClick(this.props.product.id);
        this.props.rerenderContent();
        this.forceUpdate();
    }

    OnDecrementBtnClickHandler = () => {
        this.props.actions.onDecrementBtnClick(this.props.product.id);
        this.props.rerenderContent();
        this.forceUpdate();
    }

    OnRemoveProductBtnClickHandler = () => {
        this.props.actions.onRemoveItemClick(this.props.product.id);
        this.props.rerenderContent();
    }

    public render() {
        const { product } = this.props;
        return (
            <div className={styles.cartBody}>
                <div className={[styles.figure, styles.item].join(' ')}>
                    {/* IMAGE */}
                    <Image fluid src={product.details.listImage} />
                </div>
                <div className={[styles.description, styles.item].join(' ')}>
                    {/* TITLE  add Link*/}
                    <h6>
                        <Link to={'/offer/' + product.id}>
                            {product.title}
                        </Link>
                    </h6>
                </div>
                <div className={[styles.item, styles.itemCenter].join(' ')}>
                    <span>Price:</span>
                    <div className={styles.expander} />
                    <h6><strong>
                        €{product.pricing.toFixed(2)}
                    </strong></h6>
                </div>
                <div className={[styles.item, styles.itemCenter].join(' ')}>
                    <span>Number:</span>
                    <div className={styles.expander} />
                    <div className={styles.numberItem}>
                        <div
                            className={[styles.numberField, styles.numberBtn].join(' ')}
                            onClick={this.OnDecrementBtnClickHandler}
                        >
                            <b>-</b>
                        </div>

                        <input
                            type='text'
                            value={product.cartQuantity.toString()}
                            onChange={(event) => Function()}
                            className={[styles.numberField, styles.numberInput, styles.itemCenter].join(' ')} />

                        <div
                            className={[styles.numberField, styles.numberBtn].join(' ')}
                            onClick={this.OnIncrementBtnClickHandler}
                        >
                            <b>+</b>
                        </div>
                    </div>
                </div>
                <div className={[styles.item, styles.itemCenter, styles.totalItem].join(' ')}>
                    <span>Total:</span>
                    <div className={styles.expander} />
                    <h6><strong>€{product.pricing * product.cartQuantity}</strong></h6>
                    <div
                        onClick={this.OnRemoveProductBtnClickHandler}>
                        <FontAwesomeIcon icon={faTimes} />
                    </div>
                </div>
            </div>
        );
    }
}
