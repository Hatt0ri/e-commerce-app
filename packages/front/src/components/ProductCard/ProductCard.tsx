import * as React from 'react';
import { Card, Image, Fade } from 'react-bootstrap';
import MainButton from '../../shared/components/MainButton/MainButton';
import * as styles from './productCard.module.scss';
import { IProductListInnerProps } from '../ProductList/ProductListInner';
import { Link } from 'react-router-dom';
import { Product } from '../../shared/dataInterfaces/IProduct';

interface IProductCardProps {
    viewMode?: IProductListInnerProps['viewMode'];
    product: Product;
}

interface IProductCardState {
    show: boolean;
}

export default class ProductCard extends React.PureComponent<IProductCardProps, IProductCardState> {
    constructor(props: IProductCardProps) {
        super(props);
        this.state = {
            show: true,
        };
    }
    static defaultProps: IProductCardProps = {
        viewMode: 'grid',
        product: {
            id: 'test',
            pricing: 0.01,
            sku: 'test',
            title: 'testTitle',
            quantity: 0,
            type: 'test',
            details: {
                gallery: [],
                keywords: [],
                listImage: '',
            },
        },
    };

    public render() {
        const { product } = this.props;
        return (
            <Fade
                appear
                in={this.state.show}
                timeout={150}
            >
                <Card className={styles.card}>
                    <div className={styles.imgWrapper}>
                        <Image fluid src={product.details.listImage} />
                    </div>
                    <Card.Body className={styles.cardBody}>
                        <Card.Title className={styles.title}>
                            <Link to={'/offer/' + product.id}>
                                {product.title}
                            </Link>
                        </Card.Title>
                        <div className={styles.price}>
                            <span>€{product.pricing.toFixed(2).replace('.', ',')}</span>
                        </div>
                        <Card.Text>
                            {!product.description ?
                                'No description present.' :
                                product.description
                            }
                        </Card.Text>
                        <div className={styles.mainBtn}>
                            <MainButton to={'/offer/' + product.id}>Details</MainButton>
                        </div>
                    </Card.Body>
                </Card>
            </Fade>
        );
    }
}
