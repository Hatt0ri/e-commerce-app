import * as React from 'react';
import * as styles from './productList.module.scss';
import { Card, Container, Col, Row, Button } from 'react-bootstrap';
import ProductCard from '../ProductCard/ProductCard';
import { Product } from '../../shared/dataInterfaces/IProduct';

export interface IViewModes {
    grid: string;
    list: string;
}
export interface IProductListInnerProps {
    viewMode: keyof IViewModes;
    products: Product[];
}

interface IProductListInnerState { }

export default class ProductListInner extends React.PureComponent<IProductListInnerProps, IProductListInnerState> {
    constructor(props: IProductListInnerProps) {
        super(props);
    }
    static defaultProps: IProductListInnerProps = {
        viewMode: 'grid',
        products: [],
    };

    public render() {
        const viewStyle = this.props.viewMode === 'grid' ? styles.gridView : styles.listView;
        return (
            <>
                <div className={[styles.inner, viewStyle].join(' ')}>
                    {this.props.products.map((product) => {
                        return <ProductCard key={product.id} viewMode={this.props.viewMode} product={product} />;
                    })}
                </div>
                <div className={styles.paginator}>

                </div>
            </>
        );
    }
}
