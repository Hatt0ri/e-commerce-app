import * as React from 'react';
import * as styles from './productList.module.scss';
import { Dropdown } from 'react-bootstrap';
import { IViewModes } from './ProductListInner';
import { MainTooltip } from '../../shared/components/MainTooltip';

interface IProductListToolbarProps {
    viewModeHandle: (viewMode: keyof IViewModes) => void;
}

interface IProductListToolbarState { }

export default class ProductListToolbar extends React.PureComponent<IProductListToolbarProps, IProductListToolbarState> {
    constructor(props: IProductListToolbarProps) {
        super(props);
    }

    public render() {
        const { viewModeHandle } = this.props;
        return (
            <div className={styles.toolbar}>
                <div className={styles.viewMode}>
                    <span>View as:</span>
                    <MainTooltip body={'Grid view'}>
                        <a id={styles.gridView} className={styles.viewModeBtn}
                        onClick={() => viewModeHandle('grid')}
                        >
                            <div className={styles.bar}></div>
                            <div className={styles.bar}></div>
                            <div className={styles.bar}></div>
                        </a>
                    </MainTooltip>
                    <MainTooltip body={'List view'}>
                        <a id={styles.listView} className={styles.viewModeBtn}
                        onClick={() => viewModeHandle('list')}
                        >
                            <div className={styles.bar}></div>
                            <div className={styles.bar}></div>
                            <div className={styles.bar}></div>
                        </a>
                    </MainTooltip>
                </div>
                <div id={styles.sortBy}>
                    <span>Sort by:</span>
                    <Dropdown>
                        <Dropdown.Toggle variant={''} id='dropdown-basic'>
                            Dropdown Button
                        </Dropdown.Toggle>

                        <Dropdown.Menu className={styles.dropdownMenu}>
                            <Dropdown.Item href='#'>Featured Items</Dropdown.Item>
                            <Dropdown.Item href='#/?sort=newest'>Newest Items</Dropdown.Item>
                            <Dropdown.Item href='#/?sort=bestselling'>Best Selling</Dropdown.Item>
                            <Dropdown.Item href='#/?sort=AZ'>A to Z</Dropdown.Item>
                            <Dropdown.Item href='#/?sort=ZA'>Z to A</Dropdown.Item>
                            <Dropdown.Item href='#/?sort=asc'>Price Ascending</Dropdown.Item>
                            <Dropdown.Item href='#/?sort=desc'>Price Descending</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        );
    }
}
