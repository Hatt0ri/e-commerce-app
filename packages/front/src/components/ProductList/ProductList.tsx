import * as React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import ProductListToolbar from './ProductListToolbar';
import ProductListInner, { IViewModes } from './ProductListInner';
import { Query } from 'react-apollo';
import query from '../../GQLQueries';
import { IGetAllProductsResponse, Product } from '../../shared/dataInterfaces/IProduct';
import SpinnerOverlay from '../../shared/components/SpinnerOverlay';
import PageAlert from '../../shared/components/PageAlert';

interface IProductListProps { }

interface IProductListState {
  viewMode: keyof IViewModes;
}

export default class ProductList extends React.PureComponent<IProductListProps, IProductListState> {
  constructor(props) {
    super(props);
    this.state = {
      viewMode: 'grid',
    };
  }

  viewModeHandle = (viewMode: keyof IViewModes) => {
    this.setState({ viewMode });
  }

  public render() {
    return (
      <Container>
        <Row>
          <Col sm={12}>
            <Query<IGetAllProductsResponse>
              query={query.GET_ALL_PRODUCTS}
            >
              {
                ({ loading, error, data }) => {
                  if (loading) { return <SpinnerOverlay />; }
                  if (error || !data) { throw new Error('Something went wrong'); }
                  if (data.getAllProducts.length === 0) {
                    return <PageAlert variant={'light'}>
                      There are no available products for sale now.
                    </PageAlert>;
                  }

                  const productArray: Product[] = data.getAllProducts;
                  return (
                    <>
                      {/* catergory toolbar */}
                      <ProductListToolbar viewModeHandle={this.viewModeHandle} />
                      {/* Inner ProductList */}
                      <ProductListInner viewMode={this.state.viewMode} products={productArray} />
                    </>
                  );
                }
              }
            </Query>
          </Col>
        </Row>
      </Container>
    );
  }
}
