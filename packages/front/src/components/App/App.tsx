import * as React from 'react';
import Header from '../Header/Header';

import MobileNav from '../MobileNav/MobileNav';
import ProductList from '../ProductList/ProductList';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ProductPage from '../ProductPage/ProductPage';
import Footer from '../Footer/Footer';
import PageContentWrapper from '../PageContentWrapper/PageContentWrapper';
import CartReduxContainer from '../CartPage/CartReduxContainer';

export interface AppState {
  isMobileMenuOpen: boolean;
}

class App extends React.PureComponent<{}, AppState> {
  constructor(props) {
    super(props);
    this.state = {
      isMobileMenuOpen: false,
    };
  }

  toggleMobileMenu = () => {
    this.setState({ isMobileMenuOpen: !this.state.isMobileMenuOpen });
  }

  closeMobileMenu = (event: React.MouseEvent<HTMLDivElement>) => {
    if (this.state.isMobileMenuOpen && event.target === event.currentTarget) {
      this.setState({ isMobileMenuOpen: false });
    }
  }

  render() {
    const closeMobileMenu = this.closeMobileMenu;
    const openClass = this.state.isMobileMenuOpen ? 'st-menu-open' : '';
    return (
      <Router>
        <>
          <div id={'st-container'} className={openClass}>
            <div className={'st-pusher'} onClick={this.closeMobileMenu}>
              <Header toggleMobileMenu={this.toggleMobileMenu} isMobileMenuOpen={this.state.isMobileMenuOpen} />
              <MobileNav open={this.state.isMobileMenuOpen} closeMobileMenu={closeMobileMenu} />
              <PageContentWrapper>
                <Switch>
                  <Route exact path='/' component={ProductList} />
                  <Route exact path='/offer/:id' component={ProductPage} />
                  <Route exact path='/cart' component={CartReduxContainer} />
                </Switch>
              </PageContentWrapper>
              {/* Footer */}
              <Footer />
            </div>
          </div>
        </>
      </Router>
    );
  }
}

export default App;
