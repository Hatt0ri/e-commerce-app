import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Container, Row, Col, Image, Form } from 'react-bootstrap';
import * as styles from './productPage.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShippingFast, faMedal, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faTwitter, faDiscord } from '@fortawesome/free-brands-svg-icons';
import Slider from 'react-slick';
import 'shared/scss/_slick_theme.scss';
import 'shared/scss/_slick.scss';
import { NextArrow, PreviousArrow } from '../../shared/components/SlickArrow';
import { IProductPageProps } from './ProductPage';
import { Product } from '../../shared/dataInterfaces/IProduct';
import price from '../../shared/price';
import { ICartProduct } from '../../CartStore/reducers';
import get from 'lodash/get';

interface IProductPageDescriptionProps extends IProductPageProps, RouteComponentProps {
    product: Product;
}

interface IProductPageDescriptionState {
    navImage: React.ReactElement;
    navBottom: React.ReactElement;
    quantity: number;
    validated: boolean;
}

export default class ProductPageDescription extends React.Component<IProductPageDescriptionProps, IProductPageDescriptionState> {
    private slider1: React.ReactElement;
    private slider2: React.ReactElement;
    constructor(props) {
        super(props);
        this.state = {
            navImage: null,
            navBottom: null,
            quantity: 1,
            validated: true,
        };
    }

    componentDidMount() {
        this.setState({
            navImage: this.slider1,
            navBottom: this.slider2,
        });
    }

    validate = (value) => {
        if (isNaN(value) || value > 99 || value < 1) {
            return false;
        }
        return true;
    }

    onQuantityChangeHandle = (event: React.FormEvent<any>) => {
        const current = event.currentTarget;
        const value = parseInt(current.value, 10);
        if (this.validate(value)) {
            this.setState({
                quantity: value,
                validated: true,
            });
        } else {
            this.setState({
                validated: false,
            });
        }
    }

    sumbitFormHandle = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (this.state.validated && !get(this.props.products, this.props.product.id)) {
            const cartProduct: ICartProduct = {
                ...this.props.product,
                addedAt: Date.now(),
                cartQuantity: this.state.quantity,
            };
            this.props.actions.onAddItemClick(cartProduct);
        }
    }

    public render() {
        const { title, details, pricing } = this.props.product;
        return (
            <Container>
                <Row>
                    <Col xs={12} md={6}>
                        <div className={styles.imgWrapper}>
                            <Slider
                                ref={slider => (this.slider1 = slider)}
                                asNavFor={this.state.navBottom}
                                fade={true}
                                prevArrow={<PreviousArrow className={'slick-prev'} />}
                                nextArrow={<NextArrow className={'slick-next'} />}
                            >
                                {details.gallery.map((item, index) => {
                                    return <Image fluid
                                        src={item}
                                        key={index}
                                    />;
                                })}
                            </Slider>
                        </div>
                    </Col>
                    <Col xs={12} md={6} className={styles.productDescription}>
                        <Form onSubmit={this.sumbitFormHandle}>
                            <div className={styles.productTitle}>
                                <h4>{title}</h4>
                            </div>
                            <div className={styles.productPrice}>
                                <div className={styles.priceSection}>
                                    <span className={styles.priceNumber}>€{price(pricing)}</span>
                                </div>
                                <div className={styles.priceSection}>
                                    <span className={styles.discoutSaveInfo}>
                                        (You save €{price(0)})
                                </span>
                                </div>
                                <span className={styles.numberLabel}>

                                    Number:
                                    <Form.Control
                                        onChange={this.onQuantityChangeHandle}
                                        required type='text' min={1} max={99} defaultValue={'1'} maxLength={2}
                                    />

                                </span>
                            </div>
                            <div>
                                <b>Availability: </b>{'In Stock'}
                            </div>
                            <div className={styles.otherInfo}>
                                <div>
                                    <FontAwesomeIcon icon={faShippingFast} size='2x' />
                                    Free Europe Shipping
                            </div>
                                <div>
                                    <FontAwesomeIcon icon={faMedal} size='2x' />
                                    2 Week Risk Free
                                </div>
                            </div>
                            <h1 className={styles.addToCartBtn}>
                                <input id='addToCartBtn' type='submit' value='Add to Cart' />
                            </h1>
                        </Form>
                        <div className={styles.shareBtnsWrapper}>
                            <strong>Share: </strong>
                            <div className={styles.shareBtns}>
                                <a href='#'>
                                    <FontAwesomeIcon icon={faFacebookF} size='1x' />
                                </a>
                            </div>
                            <div className={styles.shareBtns}>
                                <a href='#'>
                                    <FontAwesomeIcon icon={faTwitter} size='1x' />
                                </a>
                            </div>
                            <div className={styles.shareBtns}>
                                <a href='#'>
                                    <FontAwesomeIcon icon={faDiscord} size='3x' />
                                </a>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <div className={styles.bottomSlickWrapper}>
                            {/* <Slider
                                asNavFor={this.state.navImage}
                                ref={slider => (this.slider2 = slider)}
                                slidesToShow={4}
                                swipeToSlide={true}
                                adaptiveHeight={true}
                                rows={1}
                                prevArrow={<PreviousArrow className={'slick-prev'} />}
                                nextArrow={<NextArrow className={'slick-next'} />}
                            >
                                {details.gallery.map((item, index) => {
                                    return <Image fluid
                                        src={item}
                                        key={index}
                                    />;
                                })}
                            </Slider> */}
                        </div>
                    </Col>
                </Row>
            </Container >
        );
    }
}
