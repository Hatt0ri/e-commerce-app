import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import ProductPageDescription from './ProductPageDescription';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IGlobalState } from '../..';
import {
    CartActionTypes,
    addToCart,
} from '../../CartStore/actions';
import { ICart, ICartProduct } from '../../CartStore/reducers';
import { Query } from 'react-apollo';
import query from '../../GQLQueries';
import { IGetProductByIdResponse, Product } from '../../shared/dataInterfaces/IProduct';
import SpinnerOverlay from '../../shared/components/SpinnerOverlay';
import get from 'lodash/get';

// Component

interface IProductPageStateProps extends ICart { }

interface IProductPageDispatchProps {
    actions: {
        onAddItemClick: (product: ICartProduct) => void;
    };
}

export interface IProductPageProps extends IProductPageStateProps, IProductPageDispatchProps, RouteComponentProps { }

interface IProductPageState { }

class ProductPage extends React.PureComponent<IProductPageProps, IProductPageState> {
    constructor(props) {
        super(props);
    }

    public render() {
        const productId = get(this.props.match, 'params.id', null);
        // TODO: here should be validation and redirect to not found page
        return (
            <Query<IGetProductByIdResponse>
                query={query.GET_PRODUCT_BY_ID}
                variables={{ id: productId}}
            >
                {
                    ({ loading, error, data }) => {
                        if (loading) { return <SpinnerOverlay />; }
                        if (error || !data) { throw new Error('Something went wrong'); }

                        const product: Product = data.getProductById;
                        return (
                            <>
                                <ProductPageDescription {...this.props} product={product} />
                            </>
                        );
                    }
                }
            </Query>
        );
    }
}

// Container

interface IProductPageOwnProps { }

const mapStateToProps = (state: IGlobalState, ownProps: IProductPageOwnProps): IProductPageStateProps => {
    return {
        products: state.cartReducer.products,
    };
};

const mapDispatchToProps = (dispatch: Dispatch<CartActionTypes>, ownProps: IProductPageOwnProps): IProductPageDispatchProps => {
    return {
        actions: {
            onAddItemClick: (product) => {
                dispatch(addToCart(product));
            },
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ProductPage);
