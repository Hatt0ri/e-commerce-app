import * as React from 'react';
import * as styles from './header.module.scss';
import HeaderTop from './HeaderTop';
import HeaderMiddle from './HeaderMiddle';
import HeaderBottom from './HeaderBottom';

export interface HeaderProps {
    toggleMobileMenu: () => void;
    isMobileMenuOpen: boolean;
}

export default class Header extends React.PureComponent<HeaderProps, {}> {
    constructor(props: HeaderProps) {
        super(props);
    }
    render() {
        return (
            <header className={styles.header}>
                <HeaderTop toggleMobileMenu={this.props.toggleMobileMenu} isMobileMenuOpen={this.props.isMobileMenuOpen}/>
                <HeaderMiddle/>
                <HeaderBottom/>
            </header>
        );
    }
}
