import * as React from 'react';
import * as styles from './header.module.scss';
import { Container, Row, Col, Image } from 'react-bootstrap';
import logo from 'public/images/logo_transparent.png';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClipboard } from '@fortawesome/free-solid-svg-icons';

export default class HeaderMiddle extends React.Component {
    render() {
        return (
            <div className={styles.headerMiddle}>
                <Container className={styles.headerMiddleContainer}>
                    <Row>
                        <Col xs={4} sm={4} md={4} lg={4}>
                            {/* Delivery guarantee information */}
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={4}>
                            <div className={styles.headerLogo}>
                                <Link to='/'>
                                    <Image fluid src={logo} alt={'Logo'} />
                                </Link>
                            </div>
                        </Col>
                        <Col xs={4} sm={4} md={4} lg={4} className={styles.headerRightCol}>
                            {/* cart button, user profile button */}
                            <div className={styles.headerRightPanel}>
                                <div className={styles.headerRightPanelProfile}>
                                    <Link to='#'>
                                        <span>Sign in</span>
                                    </Link>
                                </div>
                                <div className={styles.headerRightPanelCartInfo}>
                                    <Link to={'/cart'}>
                                        <FontAwesomeIcon icon={faClipboard} size='2x' />
                                        <span>Cart</span>
                                    </Link>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container >
            </div>
        );
    }
}
