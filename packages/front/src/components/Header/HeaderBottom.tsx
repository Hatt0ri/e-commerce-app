import * as React from 'react';
import { Container, Row, Nav } from 'react-bootstrap';
import * as styles from './header.module.scss';
import { Link } from 'react-router-dom';

interface LinkType { to: string; text: string; disabled?: boolean; }

export default class HeaderBottom extends React.PureComponent {

    generateButton({ to, text, disabled = false, index }: { to: string; text: string; disabled?: boolean; index: number; }) {
        return (
            <Nav.Item as={'li'}
                key={index}
            >
                <Nav.Link
                    as={Link} to={to}
                    disabled={disabled}
                >
                    {text}
                </Nav.Link>
            </Nav.Item>
        );
    }

    render() {
        const navItems: LinkType[] = [
            {
                to: '/',
                text: 'Home',
            },
            {
                to: '/products',
                text: 'Products',
                disabled: true,
            },
            {
                to: '/blog',
                text: 'Blog',
                disabled: true,

            },
            {
                to: '/about',
                text: 'About',
                disabled: true,

            },
            {
                to: '/contact',
                text: 'Contact',
                disabled: true,
            },
        ];
        return (
            <Container className={styles.headerBottom}>
                <Row>
                    <Nav
                        className='justify-content-center'
                        // onSelect={selectedKey => alert(`selected ${selectedKey}`)}
                        as={'ul'}
                    >
                        {navItems.map((item: LinkType, index) => {
                            return this.generateButton({ ...item, index });
                        })}
                    </Nav>
                </Row>
            </Container>
        );
    }
}
