import * as React from 'react';
import { Component } from 'react';
import { Nav } from 'react-bootstrap';
import * as styles from './nav.module.scss';
import { Link } from 'react-router-dom';

export interface MobileNavProps {
    open: boolean;
    closeMobileMenu: (event: React.MouseEvent<HTMLDivElement>) => void;
}

export default class MobileNav extends Component<MobileNavProps, {}> {
    constructor(props: MobileNavProps) {
        super(props);
    }

    componentDidUpdate(prevProps: MobileNavProps) {
        if (prevProps.open === false && prevProps.open !== this.props.open) {
            window.scrollTo(0, 0);
        }
    }

    // Don't hide when clicked on nav
    preventHide = (event: React.MouseEvent<HTMLDivElement>) => {
        event.nativeEvent.preventDefault();
    }

    generateButton({ to, text, disabled = false, index }: { to: string; text: string; disabled?: boolean; index: number; }) {
        return (
            <Nav.Link
                disabled={disabled}
                onClick={this.props.closeMobileMenu as any}
                as={Link} to={to} key={index} eventKey={index}>{text}
            </Nav.Link>
        );
    }

    render() {
        const navItems: Array<{ to: string; text: string; disabled?: boolean; }> = [
            {
                to: '/',
                text: 'Home',
            },
            {
                to: '/products',
                text: 'Products',
                disabled: true,
            },
            {
                to: '/blog',
                text: 'Blog',
                disabled: true,
            },
            {
                to: '/about',
                text: 'About',
                disabled: true,
            },
            {
                to: '/contact',
                text: 'Contact',
                disabled: true,
            },
        ];
        return (
            <Nav
                defaultActiveKey='/home'
                className={[styles.nav1, 'st-menu'].join(' ')}
                onClick={this.preventHide}
            >
                {navItems.map((item, index) => {
                    return this.generateButton({ ...item, index });
                })}
            </Nav>
        );
    }
}
