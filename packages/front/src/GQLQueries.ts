import { gql } from 'apollo-boost';

const query = {
  GET_ALL_PRODUCTS: gql`
    query {
        getAllProducts {
          id
          type
          title
          quantity
          pricing
          details {
            keywords
            listImage
            gallery
          }
        }
      }
    `,
  GET_PRODUCT_BY_ID: gql`
    query ($id: ID!){
      getProductById(id: $id) {
        id
        type
        title
        quantity
        pricing
        details {
          keywords
          listImage
          gallery
        }
      }
    }
    `,
};

export default query;
