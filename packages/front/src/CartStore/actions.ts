import {
    ADD_TO_CART, REMOVE_FROM_CART,
    INCREMENT_PRODUCT, DECREMENT_PRODUCT,
} from './constants';
import { ICartProduct } from './reducers';

interface AddToCartAction {
    type: typeof ADD_TO_CART;
    product: ICartProduct;
}

interface RemoveFromCartAction {
    type: typeof REMOVE_FROM_CART;
    id: ICartProduct['id'];
}

interface IncrementAction {
    type: typeof INCREMENT_PRODUCT;
    id: ICartProduct['id'];
}

interface DecrementAction {
    type: typeof DECREMENT_PRODUCT;
    id: ICartProduct['id'];
}

export type CartActionTypes = AddToCartAction | RemoveFromCartAction | IncrementAction | DecrementAction;

export const addToCart = (product: ICartProduct): AddToCartAction => {
    return {
        type: ADD_TO_CART,
        product,
    };
};

export const removeFromCart = (productID: ICartProduct['id']): RemoveFromCartAction => {
    return {
        type: REMOVE_FROM_CART,
        id: productID,
    };
};

export const incrementProductNumberInCart = (productID: ICartProduct['id']): IncrementAction => {
    return {
        type: INCREMENT_PRODUCT,
        id: productID,
    };
};

export const decrementProductNumberInCart = (productID: ICartProduct['id']): DecrementAction => {
    return {
        type: DECREMENT_PRODUCT,
        id: productID,
    };
};
