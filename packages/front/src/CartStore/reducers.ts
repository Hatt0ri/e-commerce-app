import {
    ADD_TO_CART, REMOVE_FROM_CART,
    INCREMENT_PRODUCT, DECREMENT_PRODUCT,
} from './constants';
import {
    CartActionTypes,
} from './actions';

export interface ICart {
    products: {
        [key: string]: ICartProduct;
    };
}

export interface ICartProduct {
    id: string;
    title: string;
    quantity: number;
    cartQuantity: number;
    pricing: number;
    addedAt: Date | number;
    details: {
        listImage: string;
        gallery: string[];
    };
}

const initialState: ICart = {
    products: {},
};

export const cartReducer = (state: ICart = initialState, action: CartActionTypes): Partial<ICart> => {
    switch (action.type) {
        case ADD_TO_CART:
            return {
                ...{
                    products: { ...state.products, [action.product.id]: action.product },
                },
            };
        case REMOVE_FROM_CART:
            const rest = state;
            // there still be a value assigned to hashmap (key: undefined) devTools doesn't see this
            // assigning undefined is better for performance
            rest.products[action.id] = undefined;
            return {
                ...rest,
            };
        case INCREMENT_PRODUCT:
            const resultInc = { ...state };
            resultInc.products[action.id].cartQuantity++;
            return resultInc;
        case DECREMENT_PRODUCT:
            if (state.products[action.id].cartQuantity === 1) {
                const others = state;
                // there still be a value assigned to hashmap (key: undefined) devTools doesn't see this
                // assigning undefined is better for performance
                others.products[action.id] = undefined;
                return {
                    ...others,
                };
            }
            const resultDec = { ...state };
            resultDec.products[action.id].cartQuantity--;
            return resultDec;
        default:
            return state;
    }
};
