import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app/app.module';
import { INestApplication, Inject } from '@nestjs/common';
import { ProductsService } from '../src/products/products.service';

describe('Graphql-e2e-post', () => {
    let app: INestApplication;
    let productService: ProductsService;

    beforeAll(async () => {
        jest.setTimeout(30000);
        process.env.NODE_ENV = 'test';
        const moduleFixture = await Test.createTestingModule({
            imports: [AppModule],
        })
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        productService = moduleFixture.get<ProductsService>(ProductsService);
        await productService.deleteOneBySku('test123');
    });

    it('Case 1- Product createOne', () => {
        request(app.getHttpServer())
            .post('/graphql')
            .type('json/application')
            .send(JSON.stringify({
                query:
                    `mutation($product:InputProduct!){createOne(body:$product)
                    {id sku type title description quantity pricing shipping
                        {weight dimensions
                            {width height}}details
                            {listImage}}}`,
                variables: {
                    product: {
                        sku: 'test123',
                        type: 'Arkusz',
                        title: 'Arkusz A4 biały miękki',
                        quantity: 10,
                        pricing: 9.99,
                    },
                },
            }))
            .expect(200)
            .expect('Content-Type', /json/)
            .end((err, res) => {
                expect(res.body).toHaveProperty('data.sku');
            });
        // .then((result) => console.log(JSON.stringify(result)));
    });
});
