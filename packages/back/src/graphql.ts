
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export interface InputAddress {
    street: string;
    building: string;
    city: string;
    country: string;
    state: string;
    zipCode: string;
}

export interface InputDimensions {
    width: number;
    height: number;
    depth: number;
}

export interface InputProduct {
    sku: string;
    type: string;
    title: string;
    description?: string;
    quantity: number;
    pricing: number;
    shipping?: InputShipping;
    details: InputProductDetails;
}

export interface InputProductDetails {
    keywords: string[];
    listImage?: string;
    gallery: string[];
}

export interface InputShipping {
    weight: number;
    dimensions: InputDimensions;
}

export interface InputUser {
    firstname: string;
    lastname: string;
    email: string;
    login: string;
    phoneNumber: string;
    password: string;
    addresses: InputAddress[];
    credentials?: string[];
}

export interface Address {
    street: string;
    building: string;
    city: string;
    country: string;
    state: string;
    zipCode: string;
}

export interface Dimensions {
    width: number;
    height: number;
    depth: number;
}

export interface IMutation {
    createProduct(body: InputProduct): Product | Promise<Product>;
    deleteProductBySku(sku: string): Product | Promise<Product>;
    createUser(body: InputUser): User | Promise<User>;
    deleteUserById(id: string): User | Promise<User>;
}

export interface Product {
    id: string;
    sku: string;
    type: string;
    title: string;
    description?: string;
    quantity: number;
    pricing: number;
    shipping?: Shipping;
    details: ProductDetails;
}

export interface ProductDetails {
    keywords: string[];
    listImage: string;
    gallery: string[];
}

export interface IQuery {
    getProductById(id: string): Product | Promise<Product>;
    getAllProducts(): Product[] | Promise<Product[]>;
    getAllUsers(): User[] | Promise<User[]>;
    getUserById(id: string): User | Promise<User>;
    getUserByLogin(login: string): User | Promise<User>;
    getUserByEmail(email: string): User | Promise<User>;
}

export interface Shipping {
    weight: number;
    dimensions: Dimensions;
}

export interface User {
    id: string;
    firstname: string;
    lastname: string;
    email: string;
    login: string;
    phoneNumber: string;
    password: string;
    addresses: Address[];
    credentials: string[];
}
