import { Injectable } from '@nestjs/common';
import { MongooseModuleOptions, MongooseOptionsFactory } from '@nestjs/mongoose';
import { ConfigService } from '../../config/config.service';

@Injectable()
export class MongooseConfigService implements MongooseOptionsFactory {
  constructor(private readonly config: ConfigService) { }
  createMongooseOptions(): MongooseModuleOptions {
    return {
      uri:
        'mongodb://' +
        this.config.get('MONGO_INITDB_ROOT_USERNAME') + ':' +
        this.config.get('MONGO_INITDB_ROOT_PASSWORD') + '@' +
        this.config.get('MONGO_DB_INSTANCE'),
    };
  }
}
