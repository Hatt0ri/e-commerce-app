import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsModule } from '../products/products.module';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { MongooseConfigService } from './mongoose-config/mongoose-config.service';
import { UsersModule } from '../users/users.module';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';

@Module({
  imports: [ConfigModule,
    MongooseModule
      .forRootAsync({
        useClass: MongooseConfigService,
        inject: [ConfigService],
      }),
    GraphQLModule.forRoot({
      typePaths: ['src/**/*.graphql'],
      definitions: {
        path: join(process.cwd(), 'src/graphql.ts'),
      },
      context: ({ req }) => ({ req }),
      playground: true,
    }),
    ProductsModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule { }
