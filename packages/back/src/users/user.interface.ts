import { Document } from 'mongoose';

export interface IUser extends Document {
    firstname: string;
    lastname: string;
    email: string;
    login: string;
    phoneNumber: string;
    password: string;
    addresses: IAddress[];
    credentials: string[];
}

export interface IAddress extends Document {
    street: string;
    building: string;
    city: string;
    country: string;
    state: string;
    zipCode: string;
}
