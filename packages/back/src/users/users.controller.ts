import { Controller, Get, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { IUser } from './user.interface';
import { DeepPartial } from 'mongoose';

@Controller('createuser')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @Get(':email')
    async createUserByMongo(@Param('email') email: string) {
        const document: DeepPartial<IUser> = {
            firstname: 'asd',
            lastname: 'asd',
            email,
            login: email,
            phoneNumber: '4234234242',
            password: 'pass',
            addresses: [{
                street: 'street',
                building: '1',
                city: 'Opole',
                country: 'Polska',
                state: 'Opolskie',
                zipCode: '43-345',
            }],
            credentials: [],
        };

        const result = await this.usersService.createOne(document);
        return '<pre>' + JSON.stringify(result, null, 2) + '</pre>';
    }
}
