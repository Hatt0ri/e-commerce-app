import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema, AddressSchema } from './schemas/user.schema';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { UsersController } from './users.controller';

@Module({
    imports: [MongooseModule.forFeature([
        {name: 'User', schema: UserSchema},
        {name: 'Address', schema: AddressSchema},
    ])],
    providers: [UsersService, UsersResolver],
    controllers: [UsersController],
    exports: [UsersService],
})
export class UsersModule {}
