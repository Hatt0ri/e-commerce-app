import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    login: {
        type: String,
        required: true,
        unique: true,
        alias: 'username',
    },
    phoneNumber: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    addresses: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address',
    }],
    credentials: [{
        type: String,
    }],
});

export const AddressSchema = new mongoose.Schema({
    street: {
        type: String,
        required: true,
    },
    building: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,
    },
    zipCode: {
        type: String,
        required: true,
    },
});

/* export const shippingData = new mongoose.Schema({
    firstname: String,
    lastname: String,
    companyName: String,
    phoneNumber: String,
    address: String,
}); */
