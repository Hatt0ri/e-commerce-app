import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, DeepPartial } from 'mongoose';
import { IUser, IAddress } from './user.interface';
import { MongoError } from 'mongodb';
import { RequireOnlyOne } from '../interfaces/RequireOnlyOne';

interface IUserQueryArgEnum {
    login?: string;
    email?: string;
}

@Injectable()
export class UsersService {
    constructor(
        @InjectModel('User') private readonly userModel: Model<IUser>,
        @InjectModel('Address') private readonly addressModel: Model<IAddress>,
    ) { }

    async createOne(createUserDto: DeepPartial<IUser>): Promise<IUser> {
        try {
            const addressArray =
                createUserDto.addresses.length > 0 ?
                    await this.addressModel.create(createUserDto.addresses) : [];

            return await new this
                .userModel({ ...createUserDto, addresses: addressArray }).save();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async deleteOneById(id: string): Promise<IUser> {
        try {
            return await this.userModel.findByIdAndRemove(id).exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async findById(id: string): Promise<IUser> {
        try {
            return await this.userModel.findById(id)
                .populate('addresses')
                .exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async findOne(arg: RequireOnlyOne<IUserQueryArgEnum, 'email' | 'login'>): Promise<IUser> {
        try {
            return await this.userModel.findOne(arg)
                .populate('addresses')
                .exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async findAll(): Promise<IUser[]> {
        try {
            return await this.userModel.find()
                .populate('addresses')
                .exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }
}
