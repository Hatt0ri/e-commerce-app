import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { IUser } from './user.interface';

@Resolver('User')
export class UsersResolver {
    constructor(private readonly userService: UsersService) { }

    @Mutation('createUser')
    async createOne(@Args('body') body: IUser) {
        return await this.userService.createOne(body);
    }

    @Mutation('deleteUserById')
    async deleteOneById(@Args('id') id: string) {
        return await this.userService.deleteOneById(id);
    }

    @Query('getAllUsers')
    async getAll() {
        return await this.userService.findAll();
    }

    @Query('getUserById')
    async getById(@Args('id') id: string) {
        return await this.userService.findById(id);
    }

    @Query('getUserByLogin')
    async getByLogin(@Args('login') login: string) {
        return await this.userService.findOne( {login} );
    }

    @Query('getUserByEmail')
    async getByEmail(@Args('email') email: string) {
        return await this.userService.findOne( {email} );
    }

    /*     @ResolveProperty('addresses')
        async getAddresses(@Parent() user: IUser) {
            const { id } = user;
            return await this.postsService.findAll({ authorId: id });
        } */

}
