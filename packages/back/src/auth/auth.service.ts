import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { IUser } from '../users/user.interface';
import { Document } from 'mongoose';

@Injectable()
export class AuthService {
    constructor(private readonly usersService: UsersService) { }

    async validateUser(username: string, pass: string): Promise<Partial<IUser>> {
        const user = await this.usersService.findOne({ login: username });
        if (user && user.password === pass) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }
}
