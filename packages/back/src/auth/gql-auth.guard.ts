import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';

@Injectable()
export class GqlAuthGuard extends AuthGuard('local') {
  async canActivate(
    context: ExecutionContext,
  ) {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();
    await super.logIn(req);
    return await super.canActivate(
      new ExecutionContextHost([req]),
    ) as boolean;
  }

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }
}
