import {Module, Global} from '@nestjs/common';
import { ConfigService } from './config.service';
import * as path from 'path';

@Global()
@Module({
    providers: [{
        provide: ConfigService,
        useValue: new ConfigService(path.resolve(__dirname, process.env.NODE_ENV + '.env')),
    }],
    exports: [ConfigService],
})
export class ConfigModule {}
