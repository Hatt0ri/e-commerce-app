import { Resolver, Query, ResolveProperty, Parent, Args, Mutation } from '@nestjs/graphql';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';

@Resolver('Product')
export class ProductResolver {
    constructor(
        private readonly productService: ProductsService,
    ) { }

    @Query('getAllProducts')
    async getAll() {
        return await this.productService.findAll();
    }

    @Query('getProductById')
    async getOneById(@Args('id') id: string) {
        return await this.productService.findById(id);
    }

    @Mutation('createProduct')
    async createOne(@Args('body') body: CreateProductDto) {
        return await this.productService.createOne(body);
    }

    @Mutation('deleteProductBySku')
    async deleteOneBySku(@Args('sku') sku: CreateProductDto['sku']) {
        return await this.productService.deleteOneBySku(sku);
    }
    // @ResolveProperty('posts')
    // async getPosts(@Parent() author) {
    //     const { id } = author;
    //     return await this.postsService.findAll({ authorId: id });
    // }
}
