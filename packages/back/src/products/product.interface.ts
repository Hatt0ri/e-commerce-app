import { Document } from 'mongoose';

export interface IProduct extends Document {
    sku: string;
    type: string;
    title: string;
    description: string;
    quantity: number;
    pricing: number;
    shipping: IShipping;
    details: IDetails;
}

export interface IDimensions {
    width: number;
    height: number;
    depth: number;
}

export interface IShipping {
    weightGram: number;
    dimensions: IDimensions;
}

export interface IDetails {
    keywords: string[];
    listImage: string;
    gallery: string[];
}
