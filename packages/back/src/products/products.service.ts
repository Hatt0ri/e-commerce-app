import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IProduct } from './product.interface';
import { CreateProductDto } from './dto/create-product.dto';
import { MongoError } from 'mongodb';

@Injectable()
export class ProductsService {
    constructor(@InjectModel('Product') private readonly productModel: Model<IProduct>) { }

    async createOne(createProductDto: CreateProductDto): Promise<IProduct> {
        try {
            return await new this.productModel(createProductDto).save();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async findAll(): Promise<IProduct[]> {
        try {
            return await this.productModel.find().exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async findById(id: string): Promise<IProduct> {
        try {
            return await this.productModel.findById(id).exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }

    async deleteOneBySku(sku: IProduct['sku']): Promise<IProduct> {
        try {
            //  Mongoose won't find a property by alias. You need to use oroginal name! 'sku' is an alias.
            return await this.productModel.findOneAndDelete({ stock_keeping_unit: sku }).exec();
        } catch (err) {
            throw new MongoError(err);
        }
    }
}
