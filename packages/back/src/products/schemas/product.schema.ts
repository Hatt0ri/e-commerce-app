import * as mongoose from 'mongoose';

const getPrice = (num: number) => {
    return (num / 100).toFixed(2);
};

const setPrice = (num: number) => {
    return num * 100;
};

const dimensionsSchema = new mongoose.Schema({
    width: {
        type: Number,
        required: true,
        min: 1,
    },
    height: {
        type: Number,
        required: true,
        min: 1,
    },
    depth: {
        type: Number,
        required: true,
        min: 1,
    },
});

const shippingInfo = new mongoose.Schema({
    weightGram: {
        type: Number,
        min: 1,
        required: true,
    },
    // [mm]
    dimensions: {
        type: dimensionsSchema,
        required: true,
    },
});

const detailsSchema = new mongoose.Schema({
    keywords: [{ type: String, index: true }],
    listImage: String,
    gallery: [String],
});

export const ProductSchema = new mongoose.Schema({
    stock_keeping_unit: {
        type: String,
        unique: true,
        alias: 'sku',
        index: true,
        required: true,
    },
    type: {
        type: String,
        alias: 'category',
        index: true,
    },
    title: {
        type: String,
        minlength: 15,
        maxlength: 120,
        index: true,
        required: true,
    },
    description: {
        type: String,
    },
    quantity: {
        type: Number,
        min: 0,
        max: 999,
        required: true,
    },
    pricing: {
        type: Number,
        get: getPrice,
        set: setPrice,
        required: true,
    },
    shipping: shippingInfo,
    details: {
        type: detailsSchema,
        required: true,
    },
}, { timestamps: true });
