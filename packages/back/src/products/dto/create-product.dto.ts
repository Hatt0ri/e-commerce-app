export class CreateProductDto {
  readonly sku: string;
  readonly type: string;
  readonly title: string;
  readonly description?: string;
  readonly quantity: number;
  readonly pricing: number;
  readonly shipping?: {
    readonly weightGram: number;
    readonly dimensions: {
      readonly width: number;
      readonly height: number;
      readonly depth: number;
    };
  };
  readonly details: {
    readonly keywords: string[];
    readonly listImage: string;
    readonly gallery: string[];
  };
}
