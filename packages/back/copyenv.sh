#!/bin/sh
cp ./src/config/*.env ./dist/config/

if [ $? -eq 0 ]
then
    echo "Copied .env files!"
else
    echo "Did not copy .env files!"
fi
