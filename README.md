## [Demo preview](http://18.219.126.49)

[![preview](https://i.imgur.com/pNOi6pK.png)](http://18.219.126.49)

# TL;DR

Used tools and technologies

* docker (Dockerfile, docker-compose)
* vagrant
* AWS ECS
* nodejs
* expressjs
* reactjs
* redux
* nestjs
* typescript/ javasscript
* webpack
* nginx
* mongoose
* passportjs
* Apollo, GraphQL
* bootstrap
* html
* sass
* jest
* dotenv
* yarn
* VS Code
* git (git-flow)
* bash scripts

Planned to implement

* authorization
* redis for recommendations
* facebook OAuth

# E-Commerce App

An app that is still in early development.  
The goal is to create a scalable e-commerce application using Reactjs and Nestjs frameworks respectively for frontend and backend.
It will include a set of popular tools. Finally, the user should be able to finalize purchase process.  
The app is set up to work in development, test and production (AWS ECS) environments.



## What I learned 

##### Last update: 09.2019

* [NestJS](https://nestjs.com/)
* [Redux](https://redux.js.org/)
* [react-bootstrap](https://react-bootstrap.github.io/)
* e2e testing (jest)

I strengthened my knowledge of

* docker
* typescript
* react
* sass

Already planned to implement

* authorization
* redis for recommendations
* facebook OAuth

## How to run development setup on your device TODO

### Prerequisites

* [Install Docker](https://docs.docker.com/install/)
* [Install docker-compose](https://docs.docker.com/compose/install/)

In a directory `/e-commerce-app/provision/` use a following console command:

```docker-compose -f docker-compose-dev.yml up -d```

Fill database with mockup data TODO

Done. Your new app should appear under [localhost/](http://localhost/) in few moments.